<?php
/* ******************************************************************************************
    Script para configurar extensiones desde fuera del interfaz web de Elastix/FreePBX.
	- Obtiene por Get o Post los campos de la extensión a crear.
	- Se puede incluir un parámetro adicional "debug" para mostrar información de depuración.
	
	INSTALACIÓN: Copiar el script a la carpeta /var/www/html de Elastix.
	AUTOR: José Campaña.
	FECHA: 20/07/2016.
 * *******************************************************************************************/
$token= "9de3f69fd0949eefc5c78865e96c5af5";
$passwd=@trim($_REQUEST['token']);
if ($passwd == md5($token.date('Y-m-d').$_REQUEST['extdisplay'])){
    unset($_REQUEST['token']);
    
    //añadimos los parámetros recibidos por POST a $_REQUEST
    $vars=array();
    foreach ($_REQUEST as $k=>$v){
        $vars[$k]=$v;
    }

    //activamos o no los mensajes de depuración
    if (isset($_REQUEST['debug'])){
            $debug=true;
            unset($_REQUEST['debug']);
    }
    else
            $debug=false;

    if ($debug) echo "Cargamos configuracion de entorno<br>";

    if (file_exists('/var/www/html/admin/header_isp.php')){ //Versión antigua, pero con el directorio protegido.
            define("FREEPBX_IS_AUTH", "1");
            include('/var/www/html/admin/header_isp.php');
            include('/var/www/html/admin/modules/core/functions.inc.php'); 
        }
    else
        if (file_exists('/var/www/html/admin/header.php')){ //Versión antigua de elastix
            define("FREEPBX_IS_AUTH", "1");
            include('/var/www/html/admin/header.php');
            include('/var/www/html/admin/modules/core/functions.inc.php');
        }
        else {  $bootstrap_settings = array(); //Versión nueva de elastix
                $bootstrap_settings['freepbx_auth'] = false;
                if (!@include_once(getenv('FREEPBX_CONF') ? getenv('FREEPBX_CONF') : '/etc/freepbx.conf')) { 
                    include_once('/etc/asterisk/freepbx.conf'); 
                }
            }

    switch ($vars['action']){
        
        case 'add': if ($debug) echo "Dando de alta la extension ".$vars['extdisplay']."<br>";
                    if ($debug) echo "Ejecutando core_users_add...<br>";
                    core_users_add($vars,false);

                    if ($debug) echo "Ejecutando core_devices_add...<br>";
                    core_devices_add($vars['extdisplay'],'sip',$vars['devinfo_dial'],'fixed',$vars['extdisplay'],$vars['extdisplay']);

                    if ($debug) echo "Recargamos configuracion Asterisk<br>";
                    do_reload();

                    if ($debug){
                        $ext=core_users_get($vars['extdisplay']);
                        echo "<pre>";
                        print_r($ext);
                        echo "</pre>";
                    }
                    break;
    
        case 'del': if ($debug) echo "Dando de baja la extension ".$vars['extdisplay']."<br>";
                    if ($debug) echo "Ejecutando core_devices_del...<br>";
                    core_devices_del($vars['extdisplay']);

                    if ($debug) echo "Ejecutando core_users_del...<br>";
                    core_users_del($vars['extdisplay']);
                        
                    if ($debug) echo "Ejecutando core_users_cleanastdb...<br>";
                    core_users_cleanastdb($vars['extdisplay']);

                    if (function_exists('findmefollow_del')) {
                        if ($debug) echo "Ejecutando findmefollow_del...<br>";
                        findmefollow_del($vars['extdisplay']);
                    }

                    if ($debug) echo "Recargamos configuracion Asterisk<br>";
                    do_reload();
                    break;
        
        case 'edit':if ($debug) echo "Editando la extension ".$vars['extdisplay']."<br>";
                    if ($debug) echo "Ejecutando core_users_edit...<br>";
                    core_users_edit($vars['extdisplay'],$vars);
                    
                    if ($debug) echo "Ejecutando core_devices_del...<br>";
                    core_devices_del($vars['extdisplay']);
                    
                    if ($debug) echo "Ejecutando core_devices_add...<br>";
                    core_devices_add($vars['extdisplay'],'sip',$vars['devinfo_dial'],'fixed',$vars['extdisplay'],$vars['extdisplay']);
                    
                    if ($debug) echo "Recargamos configuracion Asterisk<br>";
                    do_reload();
                    break;
                    
        default: if ($debug) echo "¡No se ha especificado ninguna acción!<br>";
        }

    if ($debug) echo "Proceso terminado!<br>";
}
else
    if ($debug) echo "Token de seguridad incorrecto.<br>";

?>
