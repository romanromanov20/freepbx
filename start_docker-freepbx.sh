#!/bin/bash

# Credit -> https://github.com/BetterVoice/freeswitch-container


CIP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' freepbx)


sudo iptables -A DOCKER -t nat -p udp -m udp ! -i docker0 --dport 10000:30000 -j DNAT --to-destination $CIP:10000-30000
sudo iptables -A DOCKER -p udp -m udp -d $CIP/32 ! -i docker0 -o docker0 --dport 10000:30000 -j ACCEPT
sudo iptables -A POSTROUTING -t nat -p udp -m udp -s $CIP/32 -d $CIP/32 --dport 10000:30000 -j MASQUERADE

echo "sudo iptables -D POSTROUTING -t nat -p udp -m udp -s $CIP/32 -d $CIP/32 --dport 10000:30000 -j MASQUERADE"
echo "sudo iptables -D DOCKER -p udp -m udp -d $CIP/32 ! -i docker0 -o docker0 --dport 10000:30000 -j ACCEPT"
echo "sudo iptables -D DOCKER -t nat -p udp -m udp ! -i docker0 --dport 10000:30000 -j DNAT --to-destination $CIP:10000-30000"
