#!/bin/bash

pushd /usr/src/freepbx

# start apache+mysql+asterisk
/sbin/my_init &

sleep 15

mysqladmin -u root create asterisk
mysqladmin -u root create asteriskcdrdb
mysql -u root -e "GRANT ALL PRIVILEGES ON asterisk.* TO asterisk@localhost IDENTIFIED BY '$ASTERISK_DB_PW';"
mysql -u root -e "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO asterisk@localhost IDENTIFIED BY '$ASTERISK_DB_PW';"
mysql -u root -e "GRANT ALL PRIVILEGES ON asterisk.* TO myuser@'%' IDENTIFIED BY 'myPass1';"
mysql -u root -e "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO myuser@'%' IDENTIFIED BY 'myPass1';"
mysql -u root -e "flush privileges;"

echo root:ISPGestion1868 | chpasswd ;

./install -f -n  \
&& fwconsole ma refreshsignatures  \
&& fwconsole moduleadmin downloadinstall backup  \
 ivr  \
 donotdisturb  \
 findmefollow  \
 announcement  \
 queues \
 ringgroups \
 timeconditions \
 miscdests \
&& fwconsole restart --immediate  

amportal a ma upgrade framework
fwconsole reload
fwconsole ma upgrade core
fwconsole reload

fwconsole ma downloadinstall manager \
userman \
restapi \
callforward \
blacklist \
bulkhandler \
ucp \
asterisk-cli \
irc \
cidlookup \
miscapps \
parking \
arimanager \
fax \
asteriskinfo \
certman \
sms \
phpinfo
fwconsole reload


popd

